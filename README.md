# Ignoring Changes

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/ignoring-changes`.*

---

An example pipeline showing how to post-process a delta result to "ignore" certain changes.

This document describes how to run the sample. For concept details see: [Ignoring Changes and Creating a Merged Document](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/ignoring-changes-and-creating-a-merged-document)

## Running the Sample
The sample can be run via a run.bat batch file, so long as this is issued from the sample directory. Alternatively, as this batch file contains a single command, the command can be executed directly:

	..\..\bin\deltaxml.exe compare ignore documentA.xml documentB.xml result.xml
	
# **Note - .NET support has been deprecated as of version 10.0.0 **