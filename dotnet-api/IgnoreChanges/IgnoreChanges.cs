﻿// Copyright (c) 2015 DeltaXML Ltd.  All rights reserved.

using System;
using System.IO;
using DeltaXML.CoreS9Api;
using com.deltaxml.cores9api.config;
using net.sf.saxon.s9api;

namespace com.deltaxml.samples.IgnoreChanges
{
    class IgnoreChanges
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ignore Changes Sample");

            FileInfo input1 = new FileInfo("documentA.xml");
            FileInfo input2 = new FileInfo("documentB.xml");
            FileInfo output = new FileInfo(@"../../../../../result.xml");

            Console.WriteLine("Initializing DocumentComparator");
            DocumentComparator comparator = new DocumentComparator();
            comparator.setOutputProperty(Serializer.Property.INDENT, "yes");

            // Orphaned word processing
            comparator.ResultReadabilityOptions.OrphanedWordDetectionEnabled = false;
            comparator.ResultReadabilityOptions.ModifiedWhitespaceBehaviour = ModifiedWhitespaceBehaviour.NORMALIZE;
            comparator.ResultReadabilityOptions.ElementSplittingEnabled = false;
            comparator.LexicalPreservationConfig.PreserveIgnorableWhitespace = false;

            FilterStepHelper fsHelper = comparator.newFilterStepHelper();

            FilterChain fc = fsHelper.newSingleStepFilterChain(new FileInfo("disable-word-by-word.xsl"), "disable-word-by-word");
            comparator.setExtensionPoint(DocumentComparator.ExtensionPoint.PRE_FLATTENING, fc);

            // Create a filter chain
            FilterChain fcIgnoreChanges = fsHelper.newFilterChain();
            // Create a filter step for an xsl file, and add it to the filter chain.
            FilterStep fsMark = fsHelper.newFilterStep(new FileInfo("mark-ignore-changes.xsl"), "mark-ignore-changes");
            fcIgnoreChanges.addStep(fsMark);
            // Create two filters from provided resources, adding each to the same filter chain.
            FilterStep fsApply = fsHelper.newFilterStepFromResource("/xsl/apply-ignore-changes.xsl", "apply-ignore-changes");
            fcIgnoreChanges.addStep(fsApply);
            FilterStep fsPropagate = fsHelper.newFilterStepFromResource("/xsl/propagate-ignore-changes.xsl", "propagate-ignore-changes");
            fcIgnoreChanges.addStep(fsPropagate);
            // Add the chain comprising of the three filters to the OUTPUT_FINAL extension point.
            comparator.setExtensionPoint(DocumentComparator.ExtensionPoint.OUTPUT_FINAL, fcIgnoreChanges);

            Console.WriteLine("Performing the comparison.");
            comparator.compare(input1, input2, output);
            Console.WriteLine("Result is in " + output.FullName);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
